=== Resultats d’aprenentatge i criteris d’avaluació:

==== 1 Administra serveis de resolució de noms, analitzant-los i garantint la seguretat del servei.

===== 1.1	 Identifica i descriu escenaris en els quals sorgeix la necessitat d'un servei de resolució de noms.

===== 1.2.	Classifica els principals mecanismes de resolució de noms.

===== 1.3.	Descriu l'estructura, la nomenclatura i la funcionalitat dels sistemes de noms jeràrquics.

===== 1.4.	Instal·la i configura serveis jeràrquics de resolució de noms.

===== 1.5.	Prepara el servei per reexpedir consultes de recursos externs a un altre servidor de noms.

===== 1.6.	Prepara el servei per emmagatzemar i distribuir les respostes procedents d'altres servidors.

===== 1.7.	Afegeix registres de noms corresponents a una zona nova, amb opcions relatives a servidors de correu i àlies.

===== 1.8.	Implementa solucions de servidors de noms en adreces IP dinàmiques.

===== 1.9.	Realitza transferències de zona entre dos o més servidors.

===== 1.10.	Documenta els procediments d’instal·lació i configuració.


==== 2 Administra serveis de configuració automàtica, identificant-los i verificant la correcta assignació dels paràmetres.

===== 2.1.	Reconeix els mecanismes automatitzats de configuració dels paràmetres de xarxa i els avantatges que proporcionen.

===== 2.2.	Il·lustra els procediments i les pautes que intervenen en una sol•licitud de configuració dels paràmetres de xarxa.

===== 2.3.	Instal·la servidors de configuració dels paràmetres de xarxa.

===== 2.4.	Prepara el servei per assignar la configuració bàsica als equips d'una xarxa local.

===== 2.5.	Configura assignacions estàtiques i dinàmiques.

===== 2.6.	Integra en el servei opcions addicionals de configuració.

===== 2.7.	Documenta els procediments realitzats.

